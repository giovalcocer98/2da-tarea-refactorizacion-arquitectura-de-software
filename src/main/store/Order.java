package store;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Order {

	private Customer customer;
	private Salesman salesman;
	private Date orderedOn;
	private String deliveryStreet;
	private String deliveryCity;
	private String deliveryCountry;
	private Set<OrderItem> items;

	public Order(Customer customer, Salesman salesman, String deliveryStreet, String deliveryCity, String deliveryCountry, Date orderedOn) {
		this.customer = customer;
		this.salesman = salesman;
		this.deliveryStreet = deliveryStreet;
		this.deliveryCity = deliveryCity;
		this.deliveryCountry = deliveryCountry;
		this.orderedOn = orderedOn;
		this.items = new HashSet<OrderItem>();
	}

	public Customer getCustomer() {
		return customer;
	}

	public Salesman getSalesman() {
		return salesman;
	}

	public Date getOrderedOn() {
		return orderedOn;
	}

	public String getDeliveryStreet() {
		return deliveryStreet;
	}

	public String getDeliveryCity() {
		return deliveryCity;
	}

	public String getDeliveryCountry() {
		return deliveryCountry;
	}

	public Set<OrderItem> getItems() {
		return items;
	}

	public float calculateTotal() {
		float totalItems = 0;
		for (OrderItem item : items) {
			float totalItem=0;
			if (isAccessoriesCategory(item))
				totalItem = calculateDiscountForAccesories(item);
			if (isBikesCategory(item)) 
				totalItem = calculateDiscountForBikes(item);
			if (isCloathingCategory(item)) {
				totalItem = calculateDiscountForCloathing(item);
			}
			totalItems += totalItem;
		}
	

		return totalItems + calculateTax(totalItems) + shipping();
	}

	public float calculateDiscountForCloathing(OrderItem item) {
		float cloathingDiscount = 0;
		if (item.getQuantity() > 2) {
			cloathingDiscount = item.getProduct().getUnitPrice();
		}
		return calculateItemAmount(item) - cloathingDiscount;
	}

	public float calculateDiscountForAccesories(OrderItem item) {
		float booksDiscount = 0;
		float itemAmount = calculateItemAmount(item);
		if (itemAmount >= 100) {
			booksDiscount = itemAmount * 10 / 100;
		}
		return itemAmount - booksDiscount;

	}

	public float calculateDiscountForBikes(OrderItem item) {
		float itemAmount = calculateItemAmount(item);
		return itemAmount - itemAmount * 20 / 100;
	}

	public float calculateItemAmount(OrderItem item) {
		return item.getProduct().getUnitPrice() * item.getQuantity();
	}

	public float calculateTax(float totalItems) {
		return totalItems * 5 / 100;
	}

	public boolean isCloathingCategory(OrderItem item) {
		return item.getProduct().getCategory() == ProductCategory.Cloathing;
	}

	public boolean isBikesCategory(OrderItem item) {
		return item.getProduct().getCategory() == ProductCategory.Bikes;
	}

	public boolean isAccessoriesCategory(OrderItem item) {
		return item.getProduct().getCategory() == ProductCategory.Accessories;
	}
	public int shipping() {
		int shipping = 15;
		if (this.deliveryCountry == "USA")
			shipping = 0;
		return shipping;
	}
}
